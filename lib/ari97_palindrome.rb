# frozen_string_literal: true

require_relative "ari97_palindrome/version"

module Ari97Palindrome

  # Returns true for a palindrome, false otherwise
  def palindrome?
    unless processed_content.strip.empty?
      processed_content == processed_content.reverse
    else
      false
    end
  end

  def letters
    scan(/[a-z\d]/i).join("")
  end

  private

  #Returns content for palindrome testing
  def processed_content
    to_s.letters.downcase
  end

end



class String
  include Ari97Palindrome
end

class Integer
  include Ari97Palindrome
end
