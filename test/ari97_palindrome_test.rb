# frozen_string_literal: true

require "./test/test_helper"


class Ari97PalindromeTest < Minitest::Test
  def test_non_palindrome
    refute "apple".palindrome?
  end

  def test_literal_palindrome
    assert "racecar".palindrome?
  end

  def test_mixed_case_palindrome
    assert "RaceCar".palindrome?
  end

  def test_punctuated_palindrome
    assert "A man, a plan, a canal-Panama!".palindrome?
  end

  def test_letters
    # assert_equal "MadamImAdam", "Madam, I'm Adam".letters
    assert_equal "Madam, I'm Adam".letters,  "MadamImAdam"
  end

  def test_number_palindrome
    assert 121.palindrome?
  end

  def test_non_number_palindrome
    refute 12345.palindrome?
  end

  def test_empty_string
    refute "      ".palindrome?
  end


end
